defmodule Classic.MixProject do
  use Mix.Project

  def project do
    [
      app: :classic,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :dev_work,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :logger_file_backend],
      mod: {Classic.ClassicApp, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy, "2.7.0"},
      {:logger_file_backend, "~> 0.0.7"}
      # {:plug_cowboy, "~> 2.0"}
    ]
  end
end
