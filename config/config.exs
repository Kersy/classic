# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

# This configuration is loaded before any dependency and is restricted
# to this project. If another project depends on this project, this
# file won't be loaded nor affect the parent project. For this reason,
# if you want to provide default values for your application for
# third-party users, it should be done in your "mix.exs" file.

# You can configure your application as:

    config :classic,
        work_root: "C:\\cygwin64\\home\\mtk19a\\work\\classic\\priv\\",
        home_root: "/home/matthew/projects/classic/priv/",
        prod_root: "/usr/local/bin/classic/priv/"

    config :logger,
        backends: [{LoggerFileBackend, :error_log}]    

    config :logger, :error_log,
        path: "/home/matthew/projects/classic/priv/error.log",
        level: :info

    # config :dev_home,
    #     :root "C:\\Users\\Matth\\Work\\classic\\",
    #     :priv "priv\\"

    # config :prod,
        # :root "/home/ubuntu/projects/chip8_webservice/",
        # :priv "priv/"

# and access this configuration in your application as:
#
#     Application.get_env(:classic, :key)
#
# You can also configure a third-party app:
#
#     config :logger, level: :info
#

# It is also possible to import configuration files, relative to this
# directory. For example, you can emulate configuration per environment
# by uncommenting the line below and defining dev.exs, test.exs and such.
# Configuration from the imported file will override the ones defined
# here (which is why it is important to import them last).
#
#     import_config "#{Mix.env()}.exs"
