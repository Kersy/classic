defmodule Classic.State.RomRegistry do
    use Agent

  def start_link(_opts) do
    Agent.start_link(fn -> %{} end, name: :gamereg)
    Agent.start_link(fn -> %{} end, name: :valid_auths)
    # Agent.start_link(fn -> [] end, name: :)
  end

  def game_available(bucket, gamename) do
      Agent.get(bucket, &Map.get(&1, gamename)) > 0
  end

  def take_game(bucket, game, gamename, id) do
      n =  Agent.get(bucket, &Map.get(&1, gamename)) - 1
      Agent.update(bucket, fn m -> Map.put(m, gamename, n) end)

      Agent.update(game, fn g -> [id | g] end)
  end

  def release_game(bucket, game, gamename, id) do
    n =  Agent.get(bucket, &Map.get(&1, gamename)) + 1
    Agent.update(bucket, fn m -> Map.put(m, gamename, n) end)

    Agent.update(game, fn g -> List.delete(g, id) end)
  end

  def remove_with_id(id) do
    remove_with_key("pkmnbl",id)
    remove_with_key("sprmlnd",id)
    remove_with_key("tetris",id)
    remove_with_key("metroid",id)
    remove_with_key("castleii",id)
    remove_with_key("castadv",id)
    remove_with_key("dwmnstrs",id)
  end

  defp remove_with_key(game, id) do
    list = Agent.get(String.to_atom(game), fn list -> list end)
    if List.foldr(list,false, fn l,acc -> (l == id) || (acc) end) do
      release_game(:gamereg,String.to_atom(game),game,id)
    end
  end

  def put(bucket, key, value) do
    Agent.update(bucket, &Map.put(&1, key, value))
  end

  # def remove(bucket, key) do
  #   Agent.get_and_update(bucket, &Map.pop(&1,key))
  # end

  def add_game(bucket, key, value) do
    Agent.start_link(fn -> [] end, name: bucket)
    put(:gamereg,key,value)
  end

  ############## Auth functions ###############
  
  def add_auth(id,username) do
    Agent.update(:valid_auths, &Map.put(&1,id,username))
    # Agent.update(:valid_auths, fn auths -> [id | auths] end)
  end

  def auth_valid(id) do
    case Agent.get(:valid_auths,&Map.fetch(&1,id)) do
      {:ok, _} -> true
      :error -> false
      _ -> false
    end
  end

  def remove_auth(id) do
    Agent.update(:valid_auths, &Map.delete(&1,id))
    Map.drop(:valid_auths,id)
  end
  
  def get_username(id) do
    Agent.get(:valid_auths, &Map.get(&1,id))
  end
  # def add_auth(id) do
  #   Agent.update(:valid_auths, fn auths -> [id | auths] end)
  # end

  # def auth_valid(id) do
  #   list = Agent.get(:valid_auths, fn l -> l end)
  #   List.foldr(list, false, fn b, l -> (b == id) || (l) end) 
  # end

  # def remove_auth(id) do
  #   Agent.update(:valid_auths, fn l -> List.delete(l,id) end)
  # end

end