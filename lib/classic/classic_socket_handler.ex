defmodule Classic.SocketHandler do
  @key_filter ["a", "s", "t", "l", "u", "n", "E", "S"]

  def init(req, state) do
    session_id = :cowboy_req.parse_cookies(req)
    {_, id} = List.keyfind(session_id,"sessionid",0)
    if Classic.State.RomRegistry.auth_valid(id) do
      IO.puts "auth valid: " <> id

      {:cowboy_websocket, req, [id], %{idle_timeout: 60_000}}
    else
      IO.puts "auth not valid: "<> id
      :cowboy_req.reply(401, %{"Content-Type" => "text/html"}, "login required", req)
      {:ok,req,state}
    end
  end

  def websocket_init(state) do
    [id] = state 
    
    pid = self()
    # {:ok, sock} = :gen_tcp.connect('165.227.95.17', 60008, [:binary, active: false, packet: :raw])
    {:ok, sock} = :gen_tcp.connect('localhost', 60008, [:binary, active: false, packet: :raw])
    SocketRegistry.put(:sockreg,id,{self(),sock})

    spawn fn -> 
      Classic.Receiver.start(pid,sock,id)
    end

    {:reply, {:binary, [0]}, state}
  end

  def websocket_handle({:text, "up:"<>msg}, state) do
    [id] = state
    {pid,port} = SocketRegistry.get(:sockreg,id)
    case length(Enum.filter(@key_filter, fn x -> x == msg end)) do
      
      1 -> Classic.Emulator.EmulatorHandler.send(port, ["1",msg])

      _ ->
    end
    {:reply, {:binary, []}, state}
  end
 
  def websocket_handle({:text, "down:"<>msg}, state) do
    [id] = state
    IO.inspect msg
    {pid,port} = SocketRegistry.get(:sockreg,id)
    case length(Enum.filter(@key_filter, fn x -> x == msg end)) do
      
      1 -> Classic.Emulator.EmulatorHandler.send(port, ["0",msg])

      _ ->
    end

    {:reply, {:binary, []}, state}
  end

  def websocket_handle({:text, "3:"<>msg}, state) do
    [id] = state
    IO.inspect msg
    {pid,port} = SocketRegistry.get(:sockreg,id)
    Classic.Emulator.EmulatorHandler.send(port, ["3",msg])
    {:reply, {:binary, []}, state}
  end

  def websocket_handle({:text, "host"}, state) do
    [id] = state
    {pid,port} = SocketRegistry.get(:sockreg,id)
    Classic.Emulator.EmulatorHandler.send(port, ["2"])
    {:reply, {:binary, []}, state}
  end

  def websocket_handle({:text, "load:"<>msg}, state) do
    [id] = state
    {pid,port} = SocketRegistry.get(:sockreg,id)
    len = String.length(msg)
    Classic.Emulator.EmulatorHandler.send(port, ["l", Integer.to_string(len), msg])
    {:reply, {:binary, []}, state}
  end

  def websocket_handle({:text, _}, state) do
    list = Classic.Emulator.EmulatorHandler.listen(SocketRegistry.get(:sockreg,self()))
    {:reply, {:binary, list}, state}
  end

  def websocket_handle({:text, msg}, state) do
    {:ok, state}
  end

  def websocket_info(any, list, state) do
    {:reply, {:binary, list}, state}
  end

  def websocket_info(any, state) do
    {:reply, {:binary, any}, state}
  end

end
