defmodule SocketRegistry do
  use Agent

  def start_link(_opts) do
    Agent.start_link(fn -> %{} end, name: :sockreg)
  end

  @doc """
  Gets a value from the `bucket` by `key`.
  """
  def get(bucket, key) do #key will be a touple of {pid, socket}
    Agent.get(bucket, &Map.get(&1, key))
  end

  def get_keys(bucket) do #don't think I actually need this anymore
    Agent.get(bucket, &Map.keys(&1))
  end

  @doc """
  Puts the `value` for the given `key` in the `bucket`.
  """
  def put(bucket, key, value) do
    Agent.update(bucket, &Map.put(&1, key, value))
    # IO.inspect Agent.get(bucket, &Map.get(&1, key))
  end

  def remove(bucket, key) do
    # Agent.update(bucket,List.delete(Agent.get(bucket, &Map.keys(&1)),key))
    Agent.get_and_update(bucket, &Map.pop(&1,key))
  end

end
