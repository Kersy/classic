defmodule Classic.Emulator.EmulatorHandler do
  use Bitwise

  def start() do #do I need to do anything here..?
    # {:ok, sock} = :gen_tcp.connect('localhost', 60007, [:binary, active: false, packet: :raw])
    # IO.inspect Process.info(sock)
    # {:ok, sock} = :gen_tcp.connect('localhost', 60007, [:binary, active: false])
  end

  def listen(socket, list) do
    zstream = :zlib.open
    :zlib.inflateInit(zstream)
    port = SocketRegistry.get(:sockreg,socket)
    case :gen_tcp.recv(port,2) do #should be 23040
      {:error, :timeout} -> 
        SocketRegistry.remove(:sockreg,socket)
        list
      {:error, :closed} ->
        SocketRegistry.remove(:sockreg,socket)
        list
      {:error, :enotconn} -> 
        SocketRegistry.remove(:sockreg,socket)
        list
      {:ok, len} -> 
        <<h,t>> = len
        size = (h <<< 8) + (t)
        # IO.puts "#{size}"
        case :gen_tcp.recv(port,(size)) do
          {:ok, display} -> 
            display
            # :zlib.inflate(zstream,display)

          _ -> []
        end
        # display
    end
  end

  def listen(sock) do
    listen(sock,[])
  end

  def send(sock, data) do
    :gen_tcp.send(sock, data)
  end

end
