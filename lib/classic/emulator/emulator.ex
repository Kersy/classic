defmodule Classic.Emulator.Emulator do
    
    def loop do
        registry = SocketRegistry.get_keys(:sockreg)
        for r <- registry, do: get_and_send_display(r)
        loop
    end

    def get_and_send_display(socket) do
        display_data = Classic.Emulator.EmulatorHandler.listen(socket)
        send(socket,display_data)
    end
end