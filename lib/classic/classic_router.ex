defmodule Classic.Router do
  require Logger
  @filepath Application.get_env :classic, :prod_root
    
    def init(req, state) do
      Logger.info("user-agent #{inspect(Map.get(req.headers,"user-agent"))}, method #{inspect(req.method)}, path #{inspect(req.path)}, peer #{inspect(req.peer)}")
      request(req.path,req,state)
    end

    def request("/",req,state) do
      case :cowboy_req.parse_cookies(req) do
        [] ->
          req_one = :cowboy_req.set_resp_cookie("sessionid",generate_session_id,req)
          home = File.read!(Path.join(@filepath,"index.html"))
          :cowboy_req.reply(200, %{"content-type" => "text/html"}, home ,req_one)
          {:ok, req_one, state}

        _ ->
          home = File.read!(Path.join(@filepath,"index.html"))
          :cowboy_req.reply(200, %{"content-type" => "text/html"}, home ,req)
          {:ok, req, state}
      end
    end
    
    def request("/load",req,state) do #need to have a return val
      uname = Classic.State.RomRegistry.get_username(parse_id_from_req(req))
      length = String.length(uname)
      Classic.Emulator.EmulatorHandler.send(parse_port_from_req(req), ["o", Integer.to_string(length), uname])

      {:ok, req, state}
    end

    def request("savestates",req,state) do
      
    end

    def request("/save",req,state) do #need to have a return val
      uname = Classic.State.RomRegistry.get_username(parse_id_from_req(req))
      length = String.length(uname)
      Classic.Emulator.EmulatorHandler.send(parse_port_from_req(req), ["m", Integer.to_string(length), uname])
      
      {:ok, req, state}
    end

    def request("/game/"<>game,req,state) do
      id = parse_id_from_req(req)
      if Classic.State.RomRegistry.auth_valid(id) do
        len = String.length(game)
        if Classic.State.RomRegistry.game_available(:gamereg,game) do
          Classic.State.RomRegistry.remove_with_id(id)
          Classic.State.RomRegistry.take_game(:gamereg,String.to_atom(game),game,id)
          Classic.Emulator.EmulatorHandler.send(parse_port_from_req(req), ["l", Integer.to_string(len), game])
        else
          :cowboy_req.reply(503, %{"Content-Type" => "text/html"}, "game unavailable at the moment", req)
        end
      end
      {:ok, req, state}
    end

    def request("/login",req,state) do
      case Classic.Controller.AuthController.auth_login(req) do
        {:ok, username} ->
          id = parse_id_from_req(req)
          Classic.State.RomRegistry.add_auth(id,username)
          request "/console", req, state
          # :cowboy_req.reply(301, %{"location" => "home.html"}, req)

        :invalid ->
          :cowboy_req.reply(401, %{"Content-Type" => "text/html"}, "invalid credentials", req)
      end
      {:ok, req, state}
    end

    def request("/console",req,state) do
      home = File.read!(Path.join(@filepath,"console.html"))
      :cowboy_req.reply(200, %{"content-type" => "text/html"}, home ,req)
      {:ok, req, state}
    end
    
    def render(filename) do
      try do
        EEx.eval_file Path.join(@filepath,"home.html")
      rescue
        e in CompileError ->
          details = Enum.reduce(Map.from_struct(e), "",
                    fn {k,v}, acc -> acc <> "<strong>#{k}:</strong> #{v}<br>" end)
          "<h1>Template Compile Error</h1>#{details}"
      end
    end

    def generate_session_id do
      Base.url_encode64(:crypto.strong_rand_bytes(20), padding: false)
    end

    defp parse_port_from_req(req) do
      session_id = :cowboy_req.parse_cookies(req)
      {_, id} = List.keyfind(session_id,"sessionid",0)
      {pid,port} = SocketRegistry.get(:sockreg,id)
      port
    end

    defp parse_id_from_req(req) do
      session_id = :cowboy_req.parse_cookies(req)
      {_, id} = List.keyfind(session_id,"sessionid",0)
      id
    end

    # defp parse_username_from_req(req) do
    #   session_id = :cowboy_req.parse_cookies(req)
    #   IO.inspect session_id
    #   {_, uname} = List.keyfind(session_id,"username",0)
    #   uname
    # end

  end
