defmodule Classic.ClassicApp do
  use Application

  def start(_type, _args) do
    SocketRegistry.start_link([])
    Classic.State.RomRegistry.start_link([])
    Classic.State.RomRegistry.add_game(:pkmnbl,"pkmnbl",10)
    Classic.State.RomRegistry.add_game(:metroid,"metroid",10)
    Classic.State.RomRegistry.add_game(:tetris,"tetris",10)
    Classic.State.RomRegistry.add_game(:sprmlnd,"sprmlnd",10)
    Classic.State.RomRegistry.add_game(:dwmnstrs, "dwmnstrs", 10)
    Classic.State.RomRegistry.add_game(:castleii, "castleii", 10)
    Classic.State.RomRegistry.add_game(:castadv, "castadv", 10)
    start_cowboy()
    opts = [strategy: :one_for_one, name: Classic.Supervisor]
    Supervisor.start_link([],opts)
    # Classic.Emulator.Emulator.loop
  end

  def start_cowboy() do
    IO.puts "Cowboy listening on port 8080"

    dispatch = :cowboy_router.compile([
              {:_, [
                {"/", Classic.Router, []},
                {"/save", Classic.Router, []},
                {"/load", Classic.Router, []},
                {"/login", Classic.Router, []},
                {"/game/[...]", Classic.Router, []},
                {"/websocket", Classic.SocketHandler, []},
                {"/sse", Classic.ServerEvent, []},
                {"/static/images/[...]", :cowboy_static, {:priv_dir, :classic, "static/images"}},
                {"/static/[...]", :cowboy_static, {:priv_dir, :classic, "static"}}
                ]}
              ])

    :cowboy.start_clear(Classic.ClassicHandler, [{:port, 8080}], %{:env => %{:dispatch => dispatch}})

  end

end
