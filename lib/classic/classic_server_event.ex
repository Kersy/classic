defmodule Classic.ServerEvent do

  def init(req, state) do

    req_one = :cowboy_req.stream_reply(200, %{"content-type" => "text/event-stream"}, req)
    # :erlang.send_after(1_000, self(), {{:message, "first msg"}, req, state})
    # send(self(), {:message, "first msg"})

    # {:ok, sock} = :gen_tcp.connect('localhost', 60007, [:binary, active: false, packet: :raw])
    # SocketRegistry.put(:sockreg,1,sock)

    info({:message, 1},req_one,state)

    # :cowboy_req.stream_events(%{:id => "1", :data => "test"}, :fin, req_one)
    # :timer.sleep(1000)

    # :cowboy_req.stream_events(%{:id => "2"}, :nofin, req_one)
    # :timer.sleep(1000)

    # :cowboy_req.stream_events(%{:id => "3"}, :fin, req_one)

    {:cowboy_loop, req, state}
  end

  def info({:message, id},req, state) do

    # Classic.Emulator.EmulatorHandler.send(SocketRegistry.get(:sockreg,1), msg)
    # list = Classic.Emulator.EmulatorHandler.listen(SocketRegistry.get(:sockreg,1))
    # IO.inspect list

    list = <<0,0,1,1,0,1,1>>
    :cowboy_req.stream_events(%{:id => Integer.to_string(id), :data => list}, :nofin, req)

    :timer.sleep(500)
    info({:message, id+1},req, state)

    {:ok, req, state}
  end

end
