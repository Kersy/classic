defmodule Classic.Receiver do
    require Logger
    use Bitwise

    def start(pid,socket,id) do
        {pid,port} = SocketRegistry.get(:sockreg,id)
        Classic.Emulator.EmulatorHandler.send(port, String.slice(id,0..3)) #send first four characters for id
        loop(pid,socket)
    end

    def loop(pid, socket) do
        case :gen_tcp.recv(socket,1) do #should be 23040
        {:error, :timeout} -> 
	    IO.puts "timeout"
            SocketRegistry.remove(:sockreg,socket)
            []
        {:error, :closed} ->
	    IO.puts "closed"
            SocketRegistry.remove(:sockreg,socket)
            []
        {:error, :enotconn} -> 
	    IO.puts "error"
            SocketRegistry.remove(:sockreg,socket)
            []
        {:ok, header} -> 
            read_frame(header,pid,socket)
        end
    end

    def read_frame(header,pid,socket) do
        case header do
            "d" -> #Display
                read_display(socket,pid,get_header_length(socket))

            "l" -> #List of hosts
                # IO.puts "reading host list"
                read_host_list(socket,pid,[],get_header_length(socket))

            _ -> IO.puts "ERROR CANNOT READ HEADER: "<>header
        end
    end

    defp get_header_length(socket) do
        case :gen_tcp.recv(socket,2) do
            {:ok, len} -> 
                <<h,t>> = len
                length = (h <<< 8) + (t)
                length

            _ -> -1
        end
    end

    defp read_display(socket,pid,length) do
        case :gen_tcp.recv(socket,(length + 1024)) do #plus 1024 bytes of audio
            {:ok, display} -> 
                send(pid,["d" | display])
                loop(pid,socket)

            _ -> []
        end
    end

    defp read_host_list(socket,pid,list,0) do
        # IO.puts "inspecting list"
        # IO.inspect ["l" | list]
        send(pid,["l" | list])
        loop(pid,socket)
    end

    defp read_host_list(socket,pid,list,length) do #when length > 0 do
        # IO.puts "host list size: #{length}"
        case :gen_tcp.recv(socket, 4) do
            {:ok, integer} -> 
                l = [integer | list]
                read_host_list(socket,pid,l,length-1)

            _ -> []
        end
    end

end
