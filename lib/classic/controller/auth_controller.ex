defmodule Classic.Controller.AuthController do
    @filepath Application.get_env :classic, :prod_root

    def auth_login(req) do
        {:ok, body, _} = :cowboy_req.read_urlencoded_body(req)
	    IO.inspect body
        [{"user", username}, {"pass", password}] = body
        if validate_creds(username,password) do
            {:ok, username}
        else
            :invalid
        end
    end

    #TODO: Let's set up some validation
    defp validate_creds(username,password) do
        #this is WAY too damn complicated
        IO.puts "username is " <> username
        IO.puts "password is " <> Base.url_encode64(password,padding: false)
        map = File.read!(@filepath<>"passwd")
        |> String.split(["\r","\n",":"])
        |> Enum.reject(fn x -> x == "" end)
        |> Enum.chunk_every(2)
        |> Enum.map(fn [x, y] -> {x, y} end)
        |> Map.new
        |> Map.get(String.downcase(username)) == Base.url_encode64(password,padding: false)
    end
end
